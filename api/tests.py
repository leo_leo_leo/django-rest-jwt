from lib2to3.pgen2 import token
import logging
from django.urls import reverse
from .models import Company,User
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

class CheckTest(APITestCase):
    refresh_token = ''
    access_token = ''

    @classmethod
    def setUpTestData(self):
        my_admin = User.objects.create_superuser('admin', 'admin@admin.com', 'password')
        print("setUpTestData: Create superadmin.")
        pass

    """Заходим админом"""
    def test_admin_auth(self):
        print("Method: test_admin_auth.")
        url = '/auth/login/'
        data = {"username": "admin","email": "admin@admin.com","password": "password"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_client_token(self):
        print("Method: test_client_token.")
        user = User.objects.get(username='admin')
        client = APIClient(enforce_csrf_checks=True)
        client.force_authenticate(user=user)
        data = {"email": "admin@admin.com","password": "password"}
        response = client.post('http://127.0.0.1:8000/api/token/',data)
        print(response.data['refresh'])
        if response.status_code == 200 and response.data['refresh']:
            self.refresh_token = response.data['refresh']
        if response.status_code == 200 and response.data['access']:
            self.access_token = response.data['access']
        assert response.status_code == 200

    """New Company"""
    def test_create_newcompany(self):
        print("Method: test_create_newcompany.")
        url = r'/api/company/'
        line = None
        if Company.objects.count()>0:
            line = Company.objects.latest('id')
        if line:
            nom = line.id + 1
        else:
            nom = 1
        companyname = "company_" + str(nom)
        companyabout = "описание компании company_" + str(nom)
        logging.info("Название компании: %s" % companyname)
        logging.info("Название компании: %s" % companyabout)
        user = User.objects.get(username='admin')
        client = APIClient(enforce_csrf_checks=True)
        client.force_authenticate(user=user)
        data = {"company_name": companyname,"company_description": companyabout,'token': self.refresh_token}
        print(self.access_token)
        response = client.post(url, data , format='json',)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Company.objects.count(),1)

    """New User"""
    def test_create_newuser(self):
        print("Method: test_create_newuser.")
        url = r'/api/user/'
        line = None
        if User.objects.count()>0:
            line = User.objects.latest('id')
        if line:
            nom = line.id + 1
        else:
            nom = 1
        username = "user_" + str(nom)
        usermail = username + "@users.com"
        userpass = "pass" + str(nom)
        user = User.objects.get(username='admin')
        client = APIClient(enforce_csrf_checks=True)
        client.force_authenticate(user=user)
        data = {"username": username,"email": usermail,"password": userpass,'token': self.refresh_token}
        response = client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(),2)

    """Logout"""
    def test_logout(self):
        print("Method: test_logout.")
        url = '/auth/logout/'
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    """User1 auth"""
    def test_user1_auth(self):
        print("Method: test_user1_auth.")
        url = '/auth/login/'
        data = {"username": "user_2","email": "user_2@users.com","password": "pass2"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)



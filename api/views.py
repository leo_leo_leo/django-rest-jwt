from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import generics
from .models import Company,User
from .serializers import UserSerializer,CompanySerializer
from .permissons import IsOwnerOrReadOnly
from rest_framework.exceptions import PermissionDenied

class CompanyViewSet(viewsets.ModelViewSet):
    """
    API endpoint, позволяющий просматривать или редактировать компании.
    """
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    permission_classes = [permissions.IsAuthenticated,permissions.IsAdminUser]

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint, позволяющий просматривать или редактировать пользователей.
    """
    
    serializer_class = UserSerializer
    permission_classes = [IsOwnerOrReadOnly]

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser:
            return User.objects.all().order_by('-created_at')
        elif user.is_authenticated:
            return User.objects.filter(owner=user)
        raise PermissionDenied()

class UserDetail(generics.RetrieveUpdateAPIView):
    model = User
    serializer_class = UserSerializer
    lookup_field = 'username'
    permission_classes = (permissions.AllowAny,)
    def get_queryset(self):
        return User.objects.filter(username=self.kwargs.get('username'))

class CompanyDetail(generics.RetrieveUpdateAPIView):
    model = Company
    serializer_class = CompanySerializer
    lookup_field = 'company_name'
    permission_classes = (permissions.AllowAny,)
    def get_queryset(self):
        return User.objects.filter(company_name=self.kwargs.get('company_name'))

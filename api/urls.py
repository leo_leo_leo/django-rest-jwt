from django.urls import path
from rest_framework.routers import SimpleRouter
from .views import CompanyViewSet,UserViewSet

router = SimpleRouter()
router.register(r'company', CompanyViewSet, basename="company")
router.register(r'user', UserViewSet, basename="user")
urlpatterns = router.urls